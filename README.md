# JLCPCB custom DRC rules KiCAD 7.99

This project aims to deliver a PCBNew template for your KiCAD 7.99 (nighly) projects. At the moment, I have provided a template for:
1. JLCPCB 1/2-layer 1oz/2oz copper rigid PCB

Any further development will be updated in this project. Please feel free to modify it and or make a merge request with your own DRC rules since I do not have time to make all the possible custom DRC rules for all the different manufacturing processes JLCPBC offers.

## Using

To use this template, I recommend you to start by renaming the `*.kicad_pcb` file to the same name as your current PCB file, since at the moment importing a custom DRC rules is not supported. We are asking the KiCAD community to implement this feature in the near future.

Alternativelly, you can also import the settings from another board (the provided template), then go to Custom DRC and copy/paste the custom DRC from the template.

## Guarantees

I do not guarantee this custom DRC rules are accurate and might have problems. Please feel free to post an issue if you find any and I'll hope to update it ASAP.

## License
This work is licensed under the same license as KiCAD's library licensing: CC BY-SA 4.0. Further information is available here:
https://creativecommons.org/licenses/by-sa/4.0/legalcode
